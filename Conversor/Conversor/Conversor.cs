﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor
{
    public partial class Conversor : Form
    {


        public Conversor()
        {
            InitializeComponent();
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                textBinario.Text = Convert.ToString(int.Parse(textDecimal.Text), 2);
                textOctal.Text = Convert.ToString(int.Parse(textDecimal.Text), 8);
                textHexadecimal.Text = Convert.ToString(int.Parse(textDecimal.Text), 16);
            }
            if (radioButton2.Checked)
            {
                textDecimal.Text = Convert.ToInt32(textBinario.Text, 2).ToString();
                textOctal.Text = Convert.ToString(int.Parse(textDecimal.Text), 8);
                textHexadecimal.Text = Convert.ToString(int.Parse(textDecimal.Text), 16);
            }
            if (radioButton3.Checked)
            {
                textDecimal.Text = Convert.ToInt32(textOctal.Text, 8).ToString();
                textBinario.Text = Convert.ToString(int.Parse(textDecimal.Text), 2);
                textHexadecimal.Text = Convert.ToString(int.Parse(textDecimal.Text), 16);
            }
            if (radioButton4.Checked)
            {
                textDecimal.Text = Convert.ToInt32(textHexadecimal.Text, 16).ToString();
                textBinario.Text = Convert.ToString(int.Parse(textDecimal.Text), 2);
                textOctal.Text = Convert.ToString(int.Parse(textDecimal.Text), 8);
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textDecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //isDigit Solo Acepta Numeros IsContol Acepta Delete,Flechas
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void textHexadecimal_KeyPress(object sender, KeyPressEventArgs e)
        {
            int asciiCode = Convert.ToInt32(e.KeyChar);
            if ((asciiCode > 96 && asciiCode < 103) || (e.KeyChar >= '0' && e.KeyChar <= '9') || (asciiCode > 64 && asciiCode < 71))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }

        private void textBinario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar < '2')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textOctal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar < '9' )
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (checked(true)){
                textDecimal.ReadOnly = true;
                textBinario.ReadOnly = true;
                textOctal.ReadOnly = false;
                textHexadecimal.ReadOnly = true;
                textDecimal.Text = "";
                textBinario.Text = "";
                textOctal.Text = "";
                textHexadecimal.Text = "";
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (checked(true))
            {
                textDecimal.ReadOnly = false;
                textBinario.ReadOnly = true;
                textOctal.ReadOnly = true;
                textHexadecimal.ReadOnly = true;
                textDecimal.Text = "";
                textBinario.Text = "";
                textOctal.Text = "";
                textHexadecimal.Text = "";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (checked(true))
            {
                textDecimal.ReadOnly = true;
                textBinario.ReadOnly = false;
                textOctal.ReadOnly = true;
                textHexadecimal.ReadOnly = true;
                textDecimal.Text = "";
                textBinario.Text = "";
                textOctal.Text = "";
                textHexadecimal.Text = "";
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (checked(true))
            {
                textDecimal.ReadOnly = true;
                textBinario.ReadOnly = true;
                textOctal.ReadOnly = true;
                textHexadecimal.ReadOnly = false;
                textDecimal.Text = "";
                textBinario.Text = "";
                textOctal.Text = "";
                textHexadecimal.Text = "";
            }
        }
    }
}
